# Pokedex

Il s'agit d'une encyclopédie recensant les créatures fictives connues éponymes. 

Il permet d'enregistrer les informations sur les Pokémon des animaux fantastiques.

L'application utilise un ensemble de bibliothèques Android Jetpack ainsi que Retrofit pour afficher les données de l'API REST. 

L'application utilise Kotlin.

Android-Studio comme environnement utilisé sous Windows

### Prerequisites

Le projet a toutes les dépendances requises dans les fichiers gradle.
Ajoutez le projet à Android Studio ou Intelij et construisez. Toutes les dépendances requises seront téléchargées et installées.

## Architecture

Le projet utilise le modèle d'architecture MVVM.

## Librairie

* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel/) - Gérer les données liées à l'interface utilisateur d'une manière consciente du cycle de vie et agir comme un canal entre les cas d'utilisation et l'interface utilisateur

* [ViewBinding](https://developer.android.com/topic/libraries/data-binding) - Bibliothèque de prise en charge qui permet de lier les composants de l'interface utilisateur dans les mises en page aux sources de données, lie les détails des caractères et les résultats de la recherche à l'interface utilisateur

* [Navigation Component](https://developer.android.com/guide/navigation/navigation-getting-started) - Le composant de navigation d'Android Jetpack aide à la mise en œuvre

navigation entre les fragments
* [Dagger Hilt](https://developer.android.com/jetpack/androidx/releases/hilt) - Pour l'injection de dépendance.

* [Paging 3](https://developer.android.com/topic/libraries/architecture/paging/v3-overview?hl=in) - Donne le droit de paginer(ordre des pages).

* [Retrofit](https://square.github.io/retrofit/) - Pour accéder à l'API Rest.

* [Kotlin Flow](https://developer.android.com/kotlin/flow) - Pour accéder aux données de manière séquentielle

* [Datastore](https://developer.android.com/topic/libraries/architecture/datastore) - Pour stocker des données dans des paires clé-valeur, dans ce cas pour stocker un booléen si la boîte de dialogue de non-responsabilité est affichée ou non.

## Screenshots
|<img src="screenshots/home.jpg" width=200/>|<img src="screenshots/stats.jpg" width=200/>|
|:----:|:----:|

## Demo
<img src="demo/gif.gif" width=400/>
